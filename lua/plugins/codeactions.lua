--local codeactions=require('codeactions')

local function open_code_action_menu()
    require("code_action_menu").open_code_action_menu()
end

local map = vim.keymap.set
local opts = { noremap=true, silent=true }

map('', '<M-Enter>', open_code_action_menu , opts)


