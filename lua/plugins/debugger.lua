local dap, dapui = require("dap"), require("dapui")

dapui.setup()
require('dap-go').setup({})
require('telescope').load_extension('dap')
require("nvim-dap-virtual-text").setup()

vim.fn.sign_define('DapBreakpoint', {text='🛑', texthl='', linehl='', numhl=''})

local map = vim.api.nvim_set_keymap
local opts = {noremap = true, silent = true}

map('n',"<F4>","<cmd>lua require'dap'.run_to_cursor()<CR>",opts)
map('n',"<F5>","<cmd>lua require'dap'.continue()<CR>",opts)
map('n',"<F8>","<cmd>lua require'dap'.step_over()<CR>",opts)
map('n',"<F7>","<cmd>lua require'dap'.step_into()<CR>",opts)
map('n',"<F12>","<cmd>lua require'dap'.step_out()<CR>",opts)
map('n',"<Leader>b","<cmd>lua require'dap'.toggle_breakpoint();<CR>",opts)
map('n',"<F32>","<cmd>lua require'dap'.toggle_breakpoint();<CR>",opts)


map('n',"<Leader>B","<cmd>lua require'dap'.set_breakpoint(vim.fn.input('Breakpoint condition: '))<CR>",opts)
map('n',"<Leader>lp","<cmd>lua require'dap'.set_breakpoint(nil, nil, vim.fn.input('Log point message: '))<CR>",opts)
map('n',"<Leader>dr","<cmd>lua require'dap'.repl.open()<CR>",opts)
map('n',"<Leader>dl","<cmd>lua require'dap'.run_last()<CR>",opts)

dap.listeners.after.event_initialized["dapui_config"] = function()
  dapui.open()
end
dap.listeners.before.event_terminated["dapui_config"] = function()
  dapui.close()
end
dap.listeners.before.event_exited["dapui_config"] = function()
  dapui.close()
end

