local ensure_packer = function()
    local fn = vim.fn
    local install_path = fn.stdpath('data') ..
                             '/site/pack/packer/start/packer.nvim'
    if fn.empty(fn.glob(install_path)) > 0 then
        fn.system({
            'git', 'clone', '--depth', '1',
            'https://github.com/wbthomason/packer.nvim', install_path
        })
        vim.cmd [[packadd packer.nvim]]
        return true
    end
    return false
end

local packer_bootstrap = ensure_packer()

return require('packer').startup(function(use)
    use 'wbthomason/packer.nvim'
    use 'nvim-lua/plenary.nvim'
    use 'ray-x/lsp_signature.nvim' -- show function signature on type
    use('onsails/lspkind-nvim')
    use {'hrsh7th/nvim-cmp', config = function() require('plugins.cmp') end}
    use 'hrsh7th/cmp-nvim-lsp'
    use 'hrsh7th/cmp-buffer'
    use 'hrsh7th/cmp-path'

    use {
        'nvim-tree/nvim-web-devicons',
        config = function() require('plugins.devicons') end
    }

    use {'nvim-telescope/telescope-dap.nvim'}
    use {
        'nvim-treesitter/nvim-treesitter',
        --      run = ':TSUpdate',
        config = function() require('plugins.treesitter') end
    }
    -- use {"olexsmir/gopher.nvim"}
    use {
        'nvim-lualine/lualine.nvim',
        config = function() require('plugins.lualine') end
    }
    use {
        'jose-elias-alvarez/null-ls.nvim',
        config = function()
            local null_ls = require("null-ls")
            null_ls.setup {sources = {null_ls.builtins.code_actions.gitsigns}}
        end
    }
    use {
        'lewis6991/gitsigns.nvim',
        config = function() require('gitsigns').setup() end
    }

    use {
        'neovim/nvim-lspconfig',
        config = function() require('plugins.lspconfig') end
    }
    use {'ray-x/go.nvim', config = function() require('go').setup() end}
    use 'ray-x/guihua.lua' -- recommended if need floating window support
    use({
        'ray-x/navigator.lua',
        requires = {
            {'ray-x/guihua.lua', run = 'cd lua/fzy && make'},
            {'neovim/nvim-lspconfig'}
        },
        config = function() require('plugins.navigator') end
    })
    --    use {
    --        'TimUntersberger/neogit',
    --        config = function() require('plugins.neogit') end
    --    }
    use {
        'akinsho/git-conflict.nvim',
        tag = "*",
        config = function() require('git-conflict').setup() end
    }
    use({
        'weilbith/nvim-code-action-menu',
        config = function() require('plugins.codeactions') end
    })

    use {
        'nvim-tree/nvim-tree.lua',
        config = function() require('plugins.nvimtree') end
    }
    use {"nvim-telescope/telescope-project.nvim"}
    use {"nvim-telescope/telescope-file-browser.nvim"}
    use {
        'nvim-telescope/telescope.nvim',
        tag = '0.1.0',
        config = function() require('plugins.telescope') end
    }
    use {
        'mhartington/formatter.nvim',
        config = function() require("plugins.formatter") end
    }

    use {
        'mfussenegger/nvim-dap',
        requires = {
            "rcarriga/nvim-dap-ui", "nvim-telescope/telescope-dap.nvim",
            "theHamsta/nvim-dap-virtual-text", "leoluz/nvim-dap-go"
        },
        config = function() require("plugins.debugger") end
    }
    use {'sindrets/diffview.nvim', requires = 'nvim-lua/plenary.nvim'}

    use {
        "EdenEast/nightfox.nvim",
        config = function() require('plugins.nightfox') end
    }

    use {
        'L3MON4D3/LuaSnip',
        after = 'friendly-snippets',
        config = function()
            require("luasnip/loaders/from_vscode").lazy_load({
                paths = {
                    '~/.local/share/nvim/site/pack/packer/start/friendly-snippets'
                }
            })
        end
    }
    use 'saadparwaiz1/cmp_luasnip'
    use 'rafamadriz/friendly-snippets'

    -- Automatically set up your configuration after cloning packer.nvim
    -- Put this at the end after all plugins
    if packer_bootstrap then require('packer').sync() end
end)
