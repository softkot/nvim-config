require('telescope').setup {
    extensions = {
        project = {
            -- base_dirs = {{path = '~/Projects', max_depth = 1}},
            hidden_files = false, -- default: false
            theme = "dropdown",
            order_by = "asc",
            search_by = "title",
            sync_with_nvim_tree = true -- default false
        }
    },
    defaults = {
        file_sorter = require('telescope.sorters').get_fzy_sorter,
        prompt_prefix = '> ',
        color_devicons = true,
        file_previewer = require('telescope.previewers').vim_buffer_cat.new,
        grep_previewer = require('telescope.previewers').vim_buffer_vimgrep.new,
        qflist_previewer = require('telescope.previewers').vim_buffer_qflist.new
    }
}

require('telescope').load_extension('dap')
require('telescope').load_extension('file_browser')
require('telescope').load_extension('project')

local map = vim.api.nvim_set_keymap
local opts = {noremap = true, silent = true}

map('n', '<leader>ff', '<cmd>Telescope find_files<CR>', opts)
map('n', '<C-e>', '<cmd>Telescope buffers<CR>', opts)
map('n', '<leader>fg', '<cmd>Telescope live_grep<CR>', opts)
map('n', '<leader>fr', '<cmd>Telescope resume<CR>', opts)
map('n', '<leader>fb', '<cmd>Telescope buffers<CR>', opts)
map('n', '<C-p>', ":lua require'telescope'.extensions.project.project{}<CR>",
    opts)
