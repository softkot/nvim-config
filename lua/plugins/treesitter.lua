require('nvim-treesitter.configs').setup {
  ensure_installed = { 'go', 'lua','help','python', 'make', 'yaml', 'sql', 'proto', 'markdown', 'gomod', 'gowork', 'regex', 'toml' },
  auto_install = true,
  highlight = {
    enable = true,
    additional_vim_regex_highlighting = false,
 },
}
